﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace CSharpWriteIniFile
{
    class Program
    {
        [DllImport("KERNEL32.DLL")]
        private static extern int WritePrivateProfileString(
            string lpAppName,
            string lpKeyName,
            string lpString,
            string lpFileName
            );

        public void SetValue(string section, string key, string value, string fileName)
        {
            WritePrivateProfileString(section, key, value, fileName);
        }

        static void Main(string[] args)
        {
            var program = new Program();
            program.SetValue("SECTION1", "KEY1", "VALUE1", ".\\sample.ini");
        }
    }
}
